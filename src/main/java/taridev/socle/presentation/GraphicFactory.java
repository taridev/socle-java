package taridev.socle.presentation;

import javax.swing.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class GraphicFactory {


    private GraphicFactory() {
        // Empeche la création d'une instance de GraphicFactory.
    }

    public static JTextField createTextField (final Object valueObject, final String propertyName) {
        JTextField textField = new JTextField(GraphicFactory.getValue(valueObject, propertyName));
        textField.addCaretListener(e -> setValue(valueObject, propertyName, ((JTextField)e.getSource()).getText()));
        return textField;
    }

    /**
     * Retourne la valeur de la propertyName sur l'objet cible valueObject
     * @param valueObject
     * @param propertyName
     * @return
     */
    private static String getValue(final Object valueObject, final String propertyName) {
        String methodName = "get" + Character.toUpperCase(propertyName.charAt(0)) + propertyName.substring(1);
        String value = "";
        try {
            Method method = valueObject.getClass().getMethod(methodName);
            Object invokeResult = method.invoke(valueObject);
            value = invokeResult == null ? "" : invokeResult.toString();
        } catch (InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * Permet d'invoquer le setter de la propriété propertyName avec en paramètre value.
     * @param valueObject l'objet cible de l'invoke
     * @param propertyName la propriété à setter
     * @param value la valeur à setter
     */
    private static void setValue(Object valueObject, final String propertyName, final String value) {
        String methodName = "set" + Character.toUpperCase(propertyName.charAt(0)) + propertyName.substring(1);
        try {
            Method method = valueObject.getClass().getMethod(methodName, String.class);
            method.invoke(valueObject, value);
        } catch (InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}

package taridev.socle.persistence;

import java.io.Serializable;

public abstract class AbstractEntity<K extends Serializable & Comparable> implements Serializable {

    private static final long serialVersionUID = -3335569653879319571L;
    protected K primaryKey;

    public AbstractEntity() {
        // Constructeur par défaut.
    }

    public void setPK(K primaryKey) {
        this.primaryKey = primaryKey;
    }

    public K getPK() {
        return primaryKey;
    }
}

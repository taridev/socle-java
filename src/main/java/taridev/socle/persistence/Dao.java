package taridev.socle.persistence;

import java.io.Serializable;
import java.util.Set;

public interface Dao<E extends AbstractEntity<K>, K extends Serializable & Comparable> {

    void create(final E entity);
    E find(K key);
    Set<E> findAll();
    Boolean update(E entity) throws DaoException;
    Boolean delete(E entity) throws DaoException;

}
